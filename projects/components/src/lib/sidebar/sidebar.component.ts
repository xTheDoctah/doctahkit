import {Component, Input, OnInit} from '@angular/core';
import {Routes} from "@angular/router";
import {NavbarPersistence} from "./navbar-persistence.interface";


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent<T extends NavbarPersistence> implements OnInit {
  @Input() isCollapsed: boolean = false;
  @Input() persistenceService!: T;


  constructor() {

  }


  getOnlyMenuItems(routes: Routes): Routes {
    return routes.filter(data => !!data.data).map(data => {
      data.path = data.path?.startsWith("/") ? data.path : "/" + data.path;
      return data;
    }) || [];
  }

  ngOnInit(): void {
  }

  toggleSubMenu(s: string) {
    this.persistenceService.toggleMenu(s);
  }
}
