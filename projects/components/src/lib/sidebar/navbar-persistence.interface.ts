import {Route, Routes} from "@angular/router";

export interface NavbarPersistence {
  get routes(): Routes;

  get subMenuStatus(): { [key: string]: boolean }

  mapAndFilter(routes: Routes): Routes;

  initializeState(routes: Routes): void;

  mergeRoutes(parent: Route, child: Route): string;

  toggleMenu($url: string): void;

}
