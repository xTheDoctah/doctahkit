import {NgModule} from '@angular/core';
import {ComponentsComponent} from './components.component';
import {SidebarComponent} from "./sidebar/sidebar.component";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {NgbCollapseModule} from "@ng-bootstrap/ng-bootstrap";


@NgModule({
  declarations: [
    ComponentsComponent,
    SidebarComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgbCollapseModule
  ],
  exports: [
    ComponentsComponent,
    SidebarComponent
  ]
})
export class ComponentsModule {
}
