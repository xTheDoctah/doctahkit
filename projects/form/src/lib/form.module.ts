import {NgModule} from '@angular/core';
import {InputComponent} from "./input/input.component";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";


@NgModule({
  declarations: [
    InputComponent
  ],
  imports: [
    FormsModule,
    CommonModule
  ],
  exports: [
    InputComponent
  ]
})
export class FormModule {
}
