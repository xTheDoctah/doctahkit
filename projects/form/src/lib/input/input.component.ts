import {Component, Input, Optional, Self} from '@angular/core';
import {v4 as uuidv4} from 'uuid';
import {ControlValueAccessor, NgControl} from "@angular/forms";
import {Utility} from "../utility";


@Component({
  selector: 'app-input',
  templateUrl: './input.component.html'
})
export class InputComponent implements ControlValueAccessor {
  readonly uniqueId: string;
  @Input() label!: string
  @Input() isPassword: boolean = false;
  labelPosition = false;

  constructor(@Self() @Optional() public control: NgControl) {
    this.control && (this.control.valueAccessor = this);
    this.uniqueId = uuidv4();
  }

  private _hideErrors: boolean = false;

  get hideErrors(): boolean {
    return this._hideErrors;
  }

  @Input()
  set hideErrors(value: boolean) {
    this._hideErrors = value;
  }

  private _value!: string;

  get value(): string {
    return this._value;
  }

  set value(value: string) {
    this._value = value;
    if (!!this._value && this._value.trim() !== '') {
      this.labelPosition = true;
    }
    this.onChange(this._value);
    this.onTouched();
  }

  private _isDisabled = false

  get isDisabled() {
    return this._isDisabled;
  }

  writeValue(obj: any) {
    this.value = obj;
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  onChange: any = () => {
  };
  onTouched: any = () => {
  };

  setDisabledState(isDisabled: boolean): void {
    this._isDisabled = isDisabled;
  }

  isInvalid(): boolean {
    return !!this.control?.errors;
  }

  getError(): string {
    return Utility.getErrors(this.control);
  }

  switch(): void {
    this.onTouched();
    if (!!this._value && this._value.trim() !== '') {
      this.labelPosition = true;
    } else {
      this.labelPosition = !this.labelPosition;
    }
  }
}
