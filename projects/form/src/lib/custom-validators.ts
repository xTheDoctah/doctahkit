import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export class CustomValidators {

  static requiredValidator = (fieldName?: string): ValidatorFn => {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.touched && !control.value) {
        return CustomValidators.generateErrorMessage(`Il campo ${fieldName ? fieldName : ""} non può essere vuoto!`);
      }
      return null;
    };
  }

  static passwordComplexityValidator = (): ValidatorFn => {
    return (control: AbstractControl): ValidationErrors | null => {
      const regex = new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\\w\\s]).{8,}$");
      if (control.touched && !regex.test(control.value) && !!control.value) {
        return CustomValidators.generateErrorMessage("La password non è compatibile con i criteri di sicurezza.");
      }
      return null;
    }
  }

  static passwordNotMatchingValidator = (passwordField: string): ValidatorFn => {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.touched && control.parent?.get(passwordField)?.value !== control.value) {
        return CustomValidators.generateErrorMessage("Le password non coincidono");
      }
      return null;
    }
  }

  static wrongCredentialValidator = (): ValidatorFn => {
    return (control: AbstractControl): ValidationErrors => {
      return {'message': 'Not Valid'};
    }
  }

  private static generateErrorMessage = (message: string): ValidationErrors => {
    return {"message": message};
  }

}

