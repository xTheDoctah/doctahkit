import {FormGroup, NgControl} from "@angular/forms";

export class Utility {
  static markAllAsTouched(formGroup: FormGroup): void {
    Object.keys(formGroup.controls).forEach(control => {
      formGroup.get(control)?.markAsTouched();
      formGroup.get(control)?.updateValueAndValidity();
    })
  };

  static getErrors(control: NgControl): string {
    return control?.errors?.['message'];
  }
}
